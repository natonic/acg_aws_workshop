## EC2 

## Lab 1a Create an EC2 Instance 
1) Under the `Build a solution.`
2) Let's take a look at the first option, `Amazon Linux 2 AMI (HVM), SSD Volume Type.` You will note that this Linux server is listed as part of the Amazon Free Tier. You can find additional information about free tier resources at https://aws.amazon.com/free 
3) Click on the select button.
4) Choose the t2.micro option listed as Free tier eligible. 
5) As this is an introductory course, we will not be configuring any other options, choose the `Review and Launch` button. You can ignore the warning as we will be deleting this instance. 
6) Click Launch
7) You will not be prompted to `Select an existing key pair or create a new pair` Take a moment to read what a key pair is, and if you have any questions make sure to ask! 
8) On the drop-down, choose `Create a new key pair.` Choose any name you would like to use.  
9) Click on `Download Key Pair` then `Launch Instance.`
10) Congratulations, you have created your first EC2 instance. If you finish this lab with time left, go ahead and explore! 


## Aditional Steps to Explore

1) Click on `How to connect to your Linux instance` and give it a try! 
2) Create another instance, and this time after you choose your instance type, click on `Configure Instance Details.` Hover over the I next to each option read through the information provided. 

## Delete your instance

1) Click on `Actions` 
2) Go down to `Instance State` and choose `Terminate`
3) Then choose `Yes, Terminate`