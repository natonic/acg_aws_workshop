# Lab 2a: Auto Scaling 

Finished lab 2 early or want more hands-on experience? Give autoscaling a try! 

## Create and Configure an Auto Scaling Group

1) Scroll down to Auto Scaling, select `Launch Configurations`.
2) Ignore defaults and click `Launch Configuration`. 
3) Choose:  `Amazon Linux 2 AMI (HVM), SSD Volume Type General-purpose t2.micro`
4) Name your EC2 instance. 
    * Leave the other options by default.
    * Under Advanced Details, paste in the provided Bash script to configure our EC2 instance as a web server.

```
#!/bin/bash
yum update -y
yum install httpd -y
service httpd start
chkconfig httpd on
#!/bin/bash ← 
yum update -y
yum install httpd -y
service httpd start
chkconfig httpd on
``` 
**Make sure to select Assign a public IP address to every instance.** 


5) Click next to move on to storage, keep defaults, and click on next to `configure security groups`. 
6) Choose `create a new security group`.
    * Allow HTTP and SSH
    * Ignore security warnings as advanced configurations are not part of the questions for the AWSCP  
7) Create a new key pair and download it somewhere safe.
    * Though you will not be SSHing in to the server during this lab feel free to give it a try after this workshop. 
8) Click Create launch configuration. 
9) Click  `Create Auto Scaling Group` using this launch configuration.
    * Default VPS
    * Add a name, make sure to choose subnets us-east-1a and us-east-1b . 
    * Under `advanced setting`, change the health check grace period to 180 to shorten the length of this lab. 
10) Next, click create an auto-scaling group, then use scaling policies to adjust the capacity of this group.  `Scale between 1 and 1 instances.`   These will be the minimum and maximum size of your group.
    * Target value 80% when CPU usage goes above 80% it will create a new instance. 
    * The instance needs 180 seconds to warm up after scaling before health checks occur. 
11) Click past notifications and Tags.
12) Create your Auto Scaling group. 

### Additional Resources 

 [ACG: Auto Scaling Video](https://acloud.guru/course/aws-certified-cloud-practitioner/learn/Cloud-Concepts-And-Technology/autoscaling/watch) 

[AWS Auto Scaling Product Page](https://aws.amazon.com/ec2/autoscaling/)

